#!/bin/bash

# Variables
RUSTDESK_VERSION="1.2.3"
RUSTDESK_DEB="rustdesk-${RUSTDESK_VERSION}-x86_64.deb"
RUSTDESK_URL="https://github.com/rustdesk/rustdesk/releases/download/${RUSTDESK_VERSION}/${RUSTDESK_DEB}"
ID_SERVER="172.232.132.62"

# Ensure system is updated and curl, xclip are installed
sudo apt update && sudo apt install -y curl xclip

echo "Checking for RustDesk installation..."
if dpkg -l | grep -qw rustdesk; then
    echo "RustDesk is already installed. Uninstalling..."
    sudo apt remove --purge -y rustdesk
else
    echo "RustDesk is not installed. Proceeding with installation."
fi

echo "Downloading RustDesk from ${RUSTDESK_URL}..."
if curl -L "$RUSTDESK_URL" -o "$RUSTDESK_DEB"; then
    echo "Download successful."
else
    echo "Download failed. Please check the URL and try again."
    exit 1
fi

echo "Installing RustDesk..."
if sudo dpkg -i "$RUSTDESK_DEB"; then
    echo "RustDesk installed successfully."
else
    echo "Error: dpkg encountered an issue installing RustDesk. Attempting to fix..."
    # Attempt to fix broken installations
    sudo apt --fix-broken install -y
    # Try the installation again
    if sudo dpkg -i "$RUSTDESK_DEB"; then
        echo "RustDesk installed successfully after fixing dependencies."
    else
        echo "Error: Failed to install RustDesk even after attempting to fix dependencies."
        exit 1
    fi
fi

echo "Starting RustDesk..."
rustdesk &

echo "Copying server ID to clipboard..."
echo "$ID_SERVER" | xclip -selection clipboard

echo "Cleaning up..."
rm -f "$RUSTDESK_DEB"
echo "Installation complete."
